package com.example.task11;

import java.util.Collections;

public class Task11Main {
	public static void main(String[] args) {
		// здесь вы можете вручную протестировать ваше решение, вызывая реализуемый
		// метод и смотря результат
		// например вот так:
		int[] arr = {7, 5, 9};
		swap(arr);
		System.out.println(java.util.Arrays.toString(arr));
	}

	static void swap(int[] arr) {
		if (arr == null || arr.length < 2) {
			return;
		}

		int k = 0;
		for (int i = 1; i < arr.length; ++i) {
			if (arr[k] >= arr[i]) {
				k = i;
			}
		}

		arr[0] = arr[0] ^ arr[k];
		arr[k] = arr[0] ^ arr[k];
		arr[0] = arr[0] ^ arr[k];
	}
}
